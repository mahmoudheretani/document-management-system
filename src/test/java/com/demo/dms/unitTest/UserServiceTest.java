package com.demo.dms.unitTest;

import com.demo.dms.repository.UserRepository;
import com.demo.dms.dto.model.UserDto;
import com.demo.dms.dto.model.UserSignUpDto;
import com.demo.dms.model.User;
import com.demo.dms.service.UserService;
import com.demo.dms.service.UserServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.modelmapper.ModelMapper;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

class UserServiceTest {

    private final static int MOCK_USER_ID = 1;

    private final static int NON_EXISTING_USER_ID = 2;

    private UserService userService;

    @BeforeEach
    void initializeDocumentService() {
        userService = createMockUserService();
    }

    @Test
    void signUpUserTest() {
        UserDto userDto = userService.signUp(new UserSignUpDto().setUsername("aaa").setPassword("Aaa@123"));
        assertThat(userDto).isNotNull();
    }

    @Test
    void getUserTest() {
        UserDto userDto = userService.getUserById(MOCK_USER_ID);
        assertThat(userDto).isNotNull();
    }

    @Test
    void getNonExistingUserTest() {
        UserDto userDto = userService.getUserById(NON_EXISTING_USER_ID);
        assertThat(userDto).isNull();
    }

    private UserService createMockUserService() {
        UserRepository userRepository = Mockito.mock(UserRepository.class);
        when(userRepository.save(any(User.class))).then(returnsFirstArg());
        when(userRepository.findById(MOCK_USER_ID)).then(invocation -> Optional.of(new User()
                .setUsername("Test user")
                .setId(MOCK_USER_ID))
        );
        return new UserServiceImpl(userRepository, new ModelMapper(), new BCryptPasswordEncoder());
    }
}
