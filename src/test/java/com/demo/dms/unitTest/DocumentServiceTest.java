package com.demo.dms.unitTest;

import com.demo.dms.dto.model.DocumentDto;
import com.demo.dms.dto.model.DocumentFormDto;
import com.demo.dms.model.Document;
import com.demo.dms.repository.DocumentRepository;
import com.demo.dms.service.DocumentService;
import com.demo.dms.service.DocumentServiceImpl;
import com.demo.dms.sharedKernal.enums.DocumentType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.modelmapper.ModelMapper;

import java.util.ArrayList;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

class DocumentServiceTest {

    private DocumentService documentService;

    private final static int MOCK_USER_ID = 1;

    private final static int NON_EXISTING_USER_ID = 2;

    private final static int MOCK_DOCUMENT_ID = 1;

    private final static int NON_EXISTING_DOCUMENT_ID = 2;

    @BeforeEach
    void initializeDocumentService() {
        documentService = createMockDocumentService();
    }

    @Test
    void getDocumentTest() {
        DocumentDto documentDto = documentService.getDocumentByIdAndUserId(MOCK_DOCUMENT_ID, MOCK_USER_ID);
        assertThat(documentDto).isNotNull();
    }

    @Test
    void getNonExistingDocumentTest() {
        DocumentDto documentDto = documentService.getDocumentByIdAndUserId(NON_EXISTING_DOCUMENT_ID, NON_EXISTING_USER_ID);
        assertThat(documentDto).isNull();
    }

    @Test
    void getDocumentWithWrongUserIdTest() {
        DocumentDto documentDto = documentService.getDocumentByIdAndUserId(MOCK_DOCUMENT_ID, NON_EXISTING_USER_ID);
        assertThat(documentDto).isNull();
    }

    @Test
    void addDocument() {
        DocumentDto documentDto = documentService.addDocument(new DocumentFormDto()
                .setTitle("Test")
                .setDescription("test description")
                .setDocumentType(DocumentType.PNG)
                .setUserId(MOCK_USER_ID)
        );
        assertThat(documentDto).isNotNull();
    }

    private DocumentService createMockDocumentService() {
        DocumentRepository documentRepository = Mockito.mock(DocumentRepository.class);
        when(documentRepository.findByUserId(MOCK_USER_ID))
                .then(invocation -> new ArrayList<DocumentDto>() {{
                    add(new DocumentDto()
                            .setId(MOCK_DOCUMENT_ID)
                            .setTitle("First document")
                            .setDescription("Dummy description"));
                }});
        when(documentRepository.findByIdAndUserId(MOCK_DOCUMENT_ID, MOCK_USER_ID)).then(invocation -> new Document()
                .setTitle("First document")
                .setDescription("Test document")
                .setId(MOCK_DOCUMENT_ID));
        when(documentRepository.save(any(Document.class))).then(returnsFirstArg());
        return new DocumentServiceImpl(documentRepository, new ModelMapper());
    }
}
