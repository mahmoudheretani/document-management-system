package com.demo.dms.unitTest;

import com.demo.dms.config.ConfigUtility;
import com.demo.dms.model.Document;
import com.demo.dms.model.User;
import com.demo.dms.repository.DocumentRepository;
import com.demo.dms.repository.UserRepository;
import com.demo.dms.service.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.modelmapper.ModelMapper;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

class SeedServiceTest {

    private SeedService seedService;

    private final static int EXPECTED_TOTAL_SEEDED_RECORDS = 5;

    @BeforeEach
    void initializeSeedService() {
        seedService = new SeedServiceImpl(createMockDocumentService()
                , createMockUserService()
                , createMockConfigUtilityService()
        );
    }

    @Test
    void seedDataTest() {
        int totalSeededData = seedService.seedData();
        assertThat(totalSeededData).isEqualTo(EXPECTED_TOTAL_SEEDED_RECORDS);
    }

    private UserService createMockUserService() {
        UserRepository userRepository = Mockito.mock(UserRepository.class);
        when(userRepository.save(any(User.class))).then(returnsFirstArg());
        return new UserServiceImpl(userRepository, new ModelMapper(), new BCryptPasswordEncoder());
    }

    private DocumentService createMockDocumentService() {
        DocumentRepository documentRepository = Mockito.mock(DocumentRepository.class);
        when(documentRepository.save(any(Document.class))).then(returnsFirstArg());
        return new DocumentServiceImpl(documentRepository, new ModelMapper());
    }

    private ConfigUtility createMockConfigUtilityService() {
        ConfigUtility configUtility = Mockito.mock(ConfigUtility.class);
        when(configUtility.getProperty("firstUserPassword")).thenReturn("Aaa@123");
        when(configUtility.getProperty("secondUserPassword")).thenReturn("Bbb@123");
        return configUtility;
    }

}
