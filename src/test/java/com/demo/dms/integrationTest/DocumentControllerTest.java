package com.demo.dms.integrationTest;

import com.demo.dms.DmsApplication;
import com.demo.dms.dto.model.DocumentDto;
import com.demo.dms.dto.model.DocumentFormDto;
import com.demo.dms.integrationTest.request.LoginDto;
import com.demo.dms.sharedKernal.enums.DocumentType;
import org.json.JSONException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.io.Resource;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertFalse;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = DmsApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Transactional
public class DocumentControllerTest {

    private final static String AUTHORIZATION_HEADER = "Authorization";

    private final static String LOGIN_URL = "/api/auth";
    private final static String GET_DOCUMENTS_URL = "/api/v1/document/documents";
    private final static String GET_DOCUMENTS_ZIPPED_TEMPLATE_URL = "/api/v1/document/zip-file/%s";
    private final static String GET_DOCUMENT_TEMPLATE_URL = "/api/v1/document/%s/%s";
    /**
     * Same URL for add (POST) and update (PUT)
     */
    private final static String SET_DOCUMENT_URL = "/api/v1/document/";

    private final static int SEEDED_FIRST_USER_ID = 1;
    private final static int SEEDED_FIRST_USER_FIRST_DOCUMENT_ID = 3;

    private final static int NON_EXISTING_USER = 10;
    private final static int NON_EXISTING_DOCUMENTS = 23;

    @LocalServerPort
    private int serverPort;

    private TestRestTemplate restTemplate;
    private HttpHeaders headers;

    @Before
    public void initializeTest() {
        restTemplate = new TestRestTemplate();
        headers = new HttpHeaders();
    }

    @Test
    public void retrieveDocumentsTest() throws JSONException {
        headers = new HttpHeaders();
        headers.add(AUTHORIZATION_HEADER, getAuthorizationToken());

        HttpEntity<String> entity = new HttpEntity<>(null, headers);
        ResponseEntity<String> response = restTemplate.exchange(
                createURLWithPort(GET_DOCUMENTS_URL),
                HttpMethod.GET, entity, String.class);
        String expected = "[{\"id\":5,\"title\":\"Third document\",\"description\":\"Dummy description\"},{\"id\":4,\"title\":\"Second document\",\"description\":\"Dummy description\"}]";
        JSONAssert.assertEquals(expected, response.getBody(), false);
    }

    @Test
    public void retrieveDocumentByIdAndUserIdTest() throws JSONException {
        headers = new HttpHeaders();
        headers.add(AUTHORIZATION_HEADER, getAuthorizationToken());

        HttpEntity<String> entity = new HttpEntity<>(null, headers);
        ResponseEntity<String> response = restTemplate.exchange(
                String.format(createURLWithPort(GET_DOCUMENT_TEMPLATE_URL), SEEDED_FIRST_USER_FIRST_DOCUMENT_ID, SEEDED_FIRST_USER_ID),
                HttpMethod.GET, entity, String.class);
        assertFalse(response.getBody().isEmpty());
    }

    @Test
    public void retrieveNonExistingDocumentByIdAndUserIdTest() {
        headers = new HttpHeaders();
        headers.add(AUTHORIZATION_HEADER, getAuthorizationToken());

        HttpEntity<String> entity = new HttpEntity<>(null, headers);
        ResponseEntity<String> response = restTemplate.exchange(
                String.format(createURLWithPort(GET_DOCUMENTS_ZIPPED_TEMPLATE_URL), NON_EXISTING_DOCUMENTS, NON_EXISTING_USER),
                HttpMethod.GET, entity, String.class);
        assertThat(HttpStatus.NOT_FOUND).isEqualTo(response.getStatusCode());
    }

    @Test
    public void retrieveUserDocumentsAsZipFile() {
        headers = new HttpHeaders();
        headers.add(AUTHORIZATION_HEADER, getAuthorizationToken());

        HttpEntity<String> entity = new HttpEntity<>(null, headers);
        ResponseEntity<Resource> response = restTemplate.exchange(
                String.format(createURLWithPort(GET_DOCUMENTS_ZIPPED_TEMPLATE_URL), SEEDED_FIRST_USER_ID),
                HttpMethod.GET, entity, Resource.class);
        assertThat(HttpStatus.OK).isEqualTo(response.getStatusCode());
    }

    @Test
    public void insertDocument() {
        headers = new HttpHeaders();
        headers.add(AUTHORIZATION_HEADER, getAuthorizationToken());

        DocumentFormDto documentFormDto = new DocumentFormDto()
                .setTitle("New document title")
                .setDocumentType(DocumentType.PNG)
                .setUserId(SEEDED_FIRST_USER_ID);
        HttpEntity<DocumentFormDto> entity = new HttpEntity<>(documentFormDto, headers);
        ResponseEntity<String> response = restTemplate.exchange(
                String.format(createURLWithPort(SET_DOCUMENT_URL)),
                HttpMethod.POST, entity, String.class);
        assertThat(HttpStatus.OK).isEqualTo(response.getStatusCode());
    }

    @Test
    public void insertDocumentWithInvalidDescription() {
        headers = new HttpHeaders();
        headers.add(AUTHORIZATION_HEADER, getAuthorizationToken());

        DocumentFormDto documentFormDto = new DocumentFormDto()
                .setTitle("New document title")
                .setDescription("shrt")
                .setDocumentType(DocumentType.PNG)
                .setUserId(SEEDED_FIRST_USER_ID);
        HttpEntity<DocumentFormDto> entity = new HttpEntity<>(documentFormDto, headers);
        ResponseEntity<String> response = restTemplate.exchange(
                String.format(createURLWithPort(SET_DOCUMENT_URL)),
                HttpMethod.POST, entity, String.class);
        assertThat(HttpStatus.BAD_REQUEST).isEqualTo(response.getStatusCode());
    }

    @Test
    public void updateDocument() {
        headers = new HttpHeaders();
        headers.add(AUTHORIZATION_HEADER, getAuthorizationToken());

        String updateTitle = "New updated title";
        DocumentFormDto documentFormDto = new DocumentFormDto()
                .setId(SEEDED_FIRST_USER_FIRST_DOCUMENT_ID)
                .setTitle(updateTitle)
                .setDocumentType(DocumentType.PNG)
                .setUserId(SEEDED_FIRST_USER_ID);
        HttpEntity<DocumentFormDto> entity = new HttpEntity<>(documentFormDto, headers);
        ResponseEntity<DocumentDto> response = restTemplate.exchange(
                String.format(createURLWithPort(SET_DOCUMENT_URL)),
                HttpMethod.POST, entity, DocumentDto.class);
        assertThat(HttpStatus.OK).isEqualTo(response.getStatusCode());
        assertThat(updateTitle).isEqualTo(response.getBody().getTitle());
    }

    private String getAuthorizationToken() {
        HttpEntity<LoginDto> entity = new HttpEntity<>(new LoginDto().setUsername("Adam").setPassword("Bbb@123"), headers);
        ResponseEntity<Void> response = restTemplate
                .exchange(createURLWithPort(LOGIN_URL),
                        HttpMethod.POST, entity, Void.class);
        return response.getHeaders().get(AUTHORIZATION_HEADER).get(0);
    }

    private String createURLWithPort(String uri) {
        return "http://localhost:" + serverPort + uri;
    }
}
