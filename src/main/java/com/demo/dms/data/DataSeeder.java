package com.demo.dms.data;

import com.demo.dms.service.SeedService;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
@Order(0)
public class DataSeeder implements ApplicationListener<ApplicationReadyEvent> {

    private final SeedService seedService;

    @Override
    public void onApplicationEvent(ApplicationReadyEvent event) {
        seedService.seedData();
    }
}
