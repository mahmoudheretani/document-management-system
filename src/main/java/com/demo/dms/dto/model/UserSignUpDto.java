package com.demo.dms.dto.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@ToString
@JsonInclude(value = JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserSignUpDto {
    @NotNull
    @Length(min = 3, max = 20, message = "Username should be between 3 to 20 characters")
    private String username;

    @NotNull
    @Length(min = 6, max = 24, message = "Password should be between 6 to 24 characters")
    private String password;
}
