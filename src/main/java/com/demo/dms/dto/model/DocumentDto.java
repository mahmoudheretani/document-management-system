package com.demo.dms.dto.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.demo.dms.sharedKernal.enums.DocumentType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.Date;

@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@ToString
@JsonInclude(value = JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class DocumentDto {
    private int id;

    @NotBlank(message = "Title is required")
    @Size(min = 5, max = 200, message = "Title should be between 5 and 200 characters")
    private String title;

    @Min(value = 5, message = "Description should be more than 5 characters")
    private String description;

    private DocumentType documentType;
    
    private Date creationDateTime;
}
