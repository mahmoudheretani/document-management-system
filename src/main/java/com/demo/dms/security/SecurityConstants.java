package com.demo.dms.security;

public interface SecurityConstants {
    String SECRET = "Rd=o4FxPvTE$@m2izM0aVwR~tfNx.sw6No<*]W)3uF3@3)gcI^N=!nF_$:IE4;.r\"";
    String TOKEN_PREFIX = "Bearer ";
    String HEADER_STRING = "Authorization";
    String SIGN_UP_URL = "/users/sign-up";
    long SEVEN_DAYS_IN_MILLISECONDS = 7 * 24 * 60 * 60 * 1000;
}
