package com.demo.dms.model;

import com.demo.dms.sharedKernal.enums.DocumentType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.hibernate.validator.constraints.Length;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotBlank;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Accessors(chain = true)
public class Document extends BaseEntity {

    @NotBlank(message = "Title is required")
    @Length(min = 5, max = 200, message = "Title should be between 5 and 200 characters")
    private String title;

    @Length(min = 5, message = "Description should be more than 5 characters")
    private String description;

    private DocumentType documentType;

    @Column(name = "user_id", insertable = false, updatable = false)
    private Integer userId;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

}
