package com.demo.dms.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Set;

@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = {"username"}))
public class User extends BaseEntity implements Serializable {
    @NotNull
    @Length(min = 3, max = 20, message = "Username should be between 3 to 20 characters")
    private String username;

    @NotNull
    private String password;

    @OneToMany(mappedBy = "user", cascade = {CascadeType.ALL})
    private Set<Document> documents;
}
