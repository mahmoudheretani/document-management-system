package com.demo.dms.service;

import com.demo.dms.config.ConfigUtility;
import com.demo.dms.dto.model.DocumentFormDto;
import com.demo.dms.dto.model.UserSignUpDto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service("seedService")
@RequiredArgsConstructor
public class SeedServiceImpl implements SeedService {
    private final DocumentService documentService;

    private final UserService userService;

    private final ConfigUtility configUtility;

    @Override
    public int seedData() {
        int seededUsers = seedUsers();
        int seededDocuments = seedDocuments();
        return seededUsers + seededDocuments;
    }

    private int seedUsers() {
        ArrayList<UserSignUpDto> users = new ArrayList<UserSignUpDto>() {{
            add(new UserSignUpDto().setUsername("Mick").setPassword(configUtility.getProperty("firstUserPassword")));
            add(new UserSignUpDto().setUsername("Adam").setPassword(configUtility.getProperty("secondUserPassword")));
        }};
        users.forEach(userService::signUp);
        return users.size();
    }

    private int seedDocuments() {
        ArrayList<DocumentFormDto> documents = new ArrayList<DocumentFormDto>() {{
            add(new DocumentFormDto()
                    .setTitle("First document")
                    .setDescription("Dummy description")
                    .setUserId(1));
            add(new DocumentFormDto()
                    .setTitle("Second document")
                    .setDescription("Dummy description")
                    .setUserId(2));
            add(new DocumentFormDto()
                    .setTitle("Third document")
                    .setDescription("Dummy description")
                    .setUserId(2));
        }};
        documents.forEach(documentService::addDocument);
        return documents.size();
    }
}
