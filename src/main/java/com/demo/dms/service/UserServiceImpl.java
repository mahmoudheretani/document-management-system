package com.demo.dms.service;

import com.demo.dms.dto.model.UserDto;
import com.demo.dms.dto.model.UserSignUpDto;
import com.demo.dms.model.User;
import com.demo.dms.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.Optional;

@Service("userService")
@RequiredArgsConstructor
public class UserServiceImpl implements UserService, UserDetailsService {

    private final UserRepository userRepository;

    private final ModelMapper modelMapper;

    private final BCryptPasswordEncoder bCryptPasswordEncoder;


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User userEntity = userRepository.findByUsername(username);
        if (userEntity == null) {
            throw new UsernameNotFoundException(username);
        }
        return new org.springframework.security.core.userdetails.User(String.valueOf(userEntity.getId()), userEntity.getPassword(), Collections.emptyList());
    }

    @Override
    public UserDto getUserById(Integer id) {
        Optional<User> userEntity = userRepository.findById(id);
        return userEntity.map(user -> modelMapper.map(user, UserDto.class)).orElse(null);
    }

    @Override
    public UserDto signUp(UserSignUpDto userSignUpDto) {
        User userEntity = new User();
        modelMapper.map(userSignUpDto, userEntity);
        userEntity.setPassword(bCryptPasswordEncoder.encode(userSignUpDto.getPassword()));

        userEntity = userRepository.save(userEntity);
        return modelMapper.map(userEntity, UserDto.class);
    }

}
