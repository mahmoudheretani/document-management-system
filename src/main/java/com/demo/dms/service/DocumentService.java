package com.demo.dms.service;

import com.demo.dms.dto.model.DocumentDto;
import com.demo.dms.dto.model.DocumentFormDto;

import java.util.Set;

public interface DocumentService {
    /**
     * Get list of documents for specific user
     *
     * @param userId
     * @return List of documents data
     */
    Set<DocumentDto> getDocuments(Integer userId);

    /**
     * Get the document by id and the user (owner) id
     *
     * @param id
     * @param userId
     * @return Document data
     */
    DocumentDto getDocumentByIdAndUserId(Integer id, Integer userId);

    /**
     * Used to add new document
     *
     * @param documentFormDto New Document data
     * @return Added document data
     */
    DocumentDto addDocument(DocumentFormDto documentFormDto);

    /**
     * Used to update a document based on Id (Provided inside the Dto)
     *
     * @param documentFormDto Updated document data
     * @return Updated document data
     */
    DocumentDto updateDocument(DocumentFormDto documentFormDto);
}
