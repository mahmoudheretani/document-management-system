package com.demo.dms.service;

import com.demo.dms.dto.model.UserDto;
import com.demo.dms.dto.model.UserSignUpDto;

public interface UserService {
    /**
     * Get user information by Id
     *
     * @param id
     * @return
     */
    UserDto getUserById(Integer id);

    /**
     * Register new user inside the system
     *
     * @param userSignUpDto user information
     * @return the newly added user
     */
    UserDto signUp(UserSignUpDto userSignUpDto);
}
