package com.demo.dms.service;

public interface SeedService {
    /**
     * Used to seed initial data for demo proposes inside database
     * @return The total number of seeded records
     */
    int seedData();
}
