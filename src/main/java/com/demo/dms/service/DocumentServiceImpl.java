package com.demo.dms.service;

import com.demo.dms.dto.model.DocumentDto;
import com.demo.dms.dto.model.DocumentFormDto;
import com.demo.dms.model.Document;
import com.demo.dms.repository.DocumentRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.Set;
import java.util.stream.Collectors;

@Service("documentService")
@RequiredArgsConstructor
public class DocumentServiceImpl implements DocumentService {

    private final DocumentRepository documentRepository;

    private final ModelMapper modelMapper;

    @Override
    public Set<DocumentDto> getDocuments(Integer userId) {
        return documentRepository.findByUserId(userId)
                .stream()
                .map(documentEntity -> modelMapper.map(documentEntity, DocumentDto.class))
                .collect(Collectors.toSet());
    }

    @Override
    public DocumentDto getDocumentByIdAndUserId(Integer id, Integer userId) {
        Document documentEntity = documentRepository.findByIdAndUserId(id, userId);
        if (documentEntity != null) {
            return modelMapper.map(documentEntity, DocumentDto.class);
        }
        return null;
    }

    @Override
    public DocumentDto addDocument(DocumentFormDto documentFormDto) {
        return setDocument(documentFormDto);
    }

    @Override
    public DocumentDto updateDocument(DocumentFormDto documentFormDto) {
        return setDocument(documentFormDto);
    }

    /**
     * Used to add or update documents as it's only a simple table
     *
     * @param documentFormDto
     * @return
     */
    private DocumentDto setDocument(DocumentFormDto documentFormDto) {
        Document documentEntity = documentRepository.findById(documentFormDto.getId()).orElseGet(Document::new);
        modelMapper.map(documentFormDto, documentEntity);
        documentEntity = documentRepository.save(documentEntity);
        return modelMapper.map(documentEntity, DocumentDto.class);
    }

}
