package com.demo.dms.sharedKernal.enums.util;

import java.text.SimpleDateFormat;
import java.util.Date;

public final class DateUtils {

    public static String formatDateTime(Date date, String pattern) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        return simpleDateFormat.format(date);
    }

    public static String formatDateTime(Date date) {
        return formatDateTime(date, "yyyy-MM-dd hh:mm:ss");
    }
}
