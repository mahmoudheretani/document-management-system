package com.demo.dms.sharedKernal.enums;

public enum DocumentType {
    PDF,
    JPG,
    PNG
}
