package com.demo.dms.repository;

import com.demo.dms.model.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

public interface UserRepository extends PagingAndSortingRepository<User, Integer> {
    @Query(value = "FROM User user WHERE user.username=:username")
    User findByUsername(@Param("username") String username);
}
