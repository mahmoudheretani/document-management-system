package com.demo.dms.repository;

import com.demo.dms.model.Document;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.util.Set;

public interface DocumentRepository extends PagingAndSortingRepository<Document, Integer> {
    @Query(value = "FROM Document document WHERE document.id=:id and document.userId=:userId")
    Document findByIdAndUserId(@Param("id") Integer id, @Param("userId") Integer userId);

    @Query(value = "FROM Document document WHERE document.userId=:userId")
    Set<Document> findByUserId(@Param("userId") Integer userId);
}
