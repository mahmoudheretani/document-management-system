package com.demo.dms.controller.v1.api;

import com.demo.dms.dto.model.DocumentDto;
import com.demo.dms.dto.model.DocumentFormDto;
import com.demo.dms.dto.model.UserDto;
import com.demo.dms.service.DocumentService;
import com.demo.dms.service.UserService;
import com.demo.dms.sharedKernal.enums.util.DateUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.CurrentSecurityContext;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.Optional;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/document")
public class DocumentController {

    private final static String ZIP_FILE_CONTENT_TYPE = "application/zip";

    private final static String CONTENT_DISPOSITION_HEADER_NAME = "Content-Disposition";

    private final DocumentService documentService;

    private final UserService userService;

    private final ObjectMapper objectMapper;

    @GetMapping("/documents")
    public ResponseEntity<Set<DocumentDto>> getDocuments(@CurrentSecurityContext(expression = "authentication.name") String userId) {
        return ResponseEntity.ok(documentService.getDocuments(Integer.valueOf(userId)));
    }

    @GetMapping("/{id}/{userId}")
    public ResponseEntity<DocumentDto> getDocument(@PathVariable("id") Integer id,
                                                   @PathVariable("userId") Integer userId) {
        return Optional
                .ofNullable(documentService.getDocumentByIdAndUserId(id, userId))
                .map(user -> ResponseEntity.ok().body(user))
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @GetMapping("/zip-file/{userId}")
    public byte[] getDocumentsAsZipFile(HttpServletResponse response, @PathVariable("userId") Integer userId) throws IOException {
        UserDto user = userService.getUserById(userId);
        if (user == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "User not found");
        }

        setZipFileResponseHeader(response, String.format("%s_documents_%s", user.getUsername(), DateUtils.formatDateTime(new Date())));

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(byteArrayOutputStream);
        ZipOutputStream zipOutputStream = new ZipOutputStream(bufferedOutputStream);

        writeUserDocumentsToZipOutput(userId, zipOutputStream);

        zipOutputStream.finish();
        zipOutputStream.flush();
        IOUtils.closeQuietly(zipOutputStream);
        IOUtils.closeQuietly(bufferedOutputStream);
        IOUtils.closeQuietly(byteArrayOutputStream);

        return byteArrayOutputStream.toByteArray();
    }

    @PostMapping(value = "/", consumes = "application/json", produces = "application/json")
    public ResponseEntity<DocumentDto> addDocument(@RequestBody @Valid DocumentFormDto documentFormDto) {
        DocumentDto documentDto = documentService.addDocument(documentFormDto);
        return ResponseEntity.ok(documentDto);
    }

    @PostMapping("/import-zip-file/{userId}")
    public ResponseEntity<Boolean> importDocuments(@PathVariable("userId") Integer userId) {
        //Dummy API to import documents
        return ResponseEntity.ok(true);
    }

    @PutMapping("/")
    public ResponseEntity<DocumentDto> updateDocument(@RequestBody @Valid DocumentFormDto documentFormDto) {
        DocumentDto documentDto = documentService.updateDocument(documentFormDto);
        return ResponseEntity.ok(documentDto);
    }

    private void setZipFileResponseHeader(HttpServletResponse response, String fileName) {
        response.setContentType(ZIP_FILE_CONTENT_TYPE);
        response.setStatus(HttpServletResponse.SC_OK);
        response.addHeader(CONTENT_DISPOSITION_HEADER_NAME, String.format("attachment; filename=\"%s.zip\"", fileName));
    }

    private void writeUserDocumentsToZipOutput(Integer userId, ZipOutputStream zipOutputStream) throws IOException {
        Set<DocumentDto> userDocuments = documentService.getDocuments(userId);
        for (DocumentDto document : userDocuments) {
            zipOutputStream.putNextEntry(new ZipEntry(document.getTitle() + ".txt"));
            byte[] documentsJsonBytes = objectMapper
                    .writeValueAsString(document)
                    .getBytes(StandardCharsets.UTF_8);
            zipOutputStream.write(documentsJsonBytes, 0, documentsJsonBytes.length);
        }
    }

}
