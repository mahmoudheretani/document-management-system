[![LinkedIn][linkedin-shield]][linkedin-url]

<p align="center">
  <h3 align="center">Document Management System</h3>

  <p align="center">
    Java Demo RESTful API project for basic document managment system
  </p>
</p>

## Table of Contents

- [About the Project](#about-the-project)
  - [Built With](#built-with)
- [Getting Started](#getting-started)
- [Documentation](#Documentation)


## About The Project

This small MVP project thats implement basic CURD operations for document records inside In-memory database

### Built With

This MVP was build using the following techniques:

- [Java](https://www.java.com/)
- [Spring boot](https://spring.io/projects/spring-boot)


[linkedin-shield]: https://img.shields.io/badge/-LinkedIn-black.svg?style=flat-square&logo=linkedin&colorB=555
[linkedin-url]: https://www.linkedin.com/in/mahmoud-heretani-693a2b117/

## Getting Started

In order to run the project you can simply use the `API` Run config using  Intellij IDE or any other java supported IDEs.

## Documentation

The API has swagger implemented inside it you can access the swagger resources using the following link:
`http://localhost:8080/swagger-resources`